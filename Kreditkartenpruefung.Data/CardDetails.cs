﻿namespace Kreditkartenpruefung.Data
{
	public class CardDetails
	{
		public string Number { get; set; }
		
		public string CVV { get; set; } 
	}
}