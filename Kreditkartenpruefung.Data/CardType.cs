﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreditkartenpruefung.Data
{
	public class CardType
	{
		public int ID { get; set; }

		public string Name { get; set; }

		public CardType(int id, string name)
		{
			ID = id;
			Name = name;
		}

		public string PicturePath
		{
			get
			{
				if (String.IsNullOrWhiteSpace(Name))
					return @".\Images\unknownCard.png";

				switch(Name)
				{
					case "Master Card":
						return @".\Images\masterCard.png";
					case "Visa":
						return @".\Images\visaCard.png";
					case "Diners Club":
						return @".\Images\dinersCard.png";
					case "Maestro":
						return @".\Images\maestroCard.png";
					default:
						return @".\Images\unknownCard.png";
				}
			}
		}
	}
}
