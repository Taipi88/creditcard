﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreditkartenpruefung.Data
{
    public class DatenbankHelfer
    {
		private const string CONNECTION_STRING = @"Data Source=LOCALHOST\SQLEXPRESS;Initial Catalog=Credit;Pooling=false;Integrated Security=True";
		private const string APP_ROLE = "Readers";
		private const string ROLE_PW = "nicole#123";

		#region PrivateMethods

		private SqlConnection GetConnection()
		{
			return new SqlConnection(CONNECTION_STRING);
		}

		private bool DoAuthorization(SqlConnection connection)
		{
			using (SqlCommand command = new SqlCommand("sp_setapprole", connection) { CommandType = System.Data.CommandType.StoredProcedure })
			{
				// wir nehmen an, dass die Connection bereits geöffnet wurde!!!
				command.Parameters.AddWithValue("rolename", APP_ROLE);
				command.Parameters.AddWithValue("password", ROLE_PW);
				bool ok = command.ExecuteNonQuery() == -1;
				return ok;
			}
		}

		private int GetOrCreateCustomer(SqlConnection connection, string customerName, string customerSurname)
		{
			using (SqlCommand command = new SqlCommand("SaveCustomer", connection) { CommandType = System.Data.CommandType.StoredProcedure })
			{
				// wir nehmen an, dass die Connection bereits geöffnet wurde!!!
				command.Parameters.AddWithValue("@Customer_Name", customerName);
				command.Parameters.AddWithValue("@Customer_Surname", customerSurname);

				// output-Parameter für Kundennummer hinzufügen
				SqlParameter output = new SqlParameter("@Customer_ID", System.Data.SqlDbType.Int);
				output.Direction = System.Data.ParameterDirection.Output;
				command.Parameters.Add(output);

				// ausführen
				command.ExecuteNonQuery();

				// kundennummer abholen
				int customerId = (int)output.Value;

				return customerId;
			}
		}

		private int SaveCard(SqlConnection connection, int customerId, int cardTypeId, string expiry)
		{
			using (SqlCommand command = new SqlCommand("SaveCard", connection) { CommandType = System.Data.CommandType.StoredProcedure })
			{
				// wir nehmen an, dass die Connection bereits geöffnet wurde!!!
				command.Parameters.AddWithValue("@Customer_ID", customerId);
				command.Parameters.AddWithValue("@Cardtype_ID", cardTypeId);
				command.Parameters.AddWithValue("@Expiry", expiry);

				// output-Parameter für Kundennummer hinzufügen
				SqlParameter output = new SqlParameter("@Card_ID", System.Data.SqlDbType.Int);
				output.Direction = System.Data.ParameterDirection.Output;
				command.Parameters.Add(output);

				// ausführen
				command.ExecuteNonQuery();

				// kundennummer abholen
				int cardId = (int)output.Value;

				return cardId;
			}
		}

		#endregion

		#region Methods

		public List<CardType> GetCardTypes()
		{
			List<CardType> types = new List<CardType>();

			using (SqlConnection connection = GetConnection())
			{
				connection.Open();

				if (DoAuthorization(connection))
				{
					string queryString = "SELECT Cardtype_ID, Cardtype_Name " +
												"FROM CardType";
					using (SqlCommand command = new SqlCommand(queryString, connection))
					{
						using (SqlDataReader reader = command.ExecuteReader()) // daten abholen
						{
							if (reader.HasRows)
							{
								while (reader.Read())
								{
									int id = reader.GetInt32(0);
									string name = reader.GetString(1);
									CardType type = new CardType(id, name);
									types.Add(type);
								}
							}
						}
					}
				}
				return types;
			}
		}

		public CardDetails SaveNewCard(string customerName, string customerSurname, int cardTypeId, string expiry)
		{
			using (SqlConnection connection = GetConnection())
			{
				connection.Open();

				if (DoAuthorization(connection))
				{
					try
					{
						// kunden-id holen
						int customerId = GetOrCreateCustomer(connection, customerName, customerSurname);
						int cardId = SaveCard(connection, customerId, cardTypeId, expiry);

						string queryString = "SELECT Card_Number, CVV " +
												"FROM Card " +
												"WHERE Card_ID = @Card_ID";
						using (SqlCommand command = new SqlCommand(queryString, connection))
						{
							command.Parameters.AddWithValue("@Card_ID", cardId);

							SqlDataReader reader = command.ExecuteReader(); // daten abholen
							if (reader.HasRows)
							{
								reader.Read(); // keine while-Schleife von Nöten, da wir nur einen Eintrag kriegen wollen
								string card_number = reader.GetString(0);
								string card_cvv = reader.GetString(1);

								// karten-details zurückgeben
								return new CardDetails() { Number = card_number, CVV = card_cvv };
							}
						}
					}
					catch (InvalidCastException) { } // tritt auf, wenn die StoredProcedure erkannt hat, dass etwas krumm ist und keine Kartennummer zurückgibt
					catch (SqlException) { } // tritt auf, wenn die CHECK-Einschränkungen der DB gestoppt haben
				}
				return null;
			}
		}

		public bool CheckCard(string customerName, string customerSurname, int cardTypeId, string expiry, string cardNumber, string cvv)
		{
			using (SqlConnection connection = GetConnection())
			{
				connection.Open();

				if (DoAuthorization(connection))
				{
					using (SqlCommand command = new SqlCommand("CheckCard", connection) { CommandType = System.Data.CommandType.StoredProcedure })
					{
						// wir nehmen an, dass die Connection bereits geöffnet wurde!!!
						command.Parameters.AddWithValue("@Customer_Name", customerName);
						command.Parameters.AddWithValue("@Customer_Surname", customerSurname);
						command.Parameters.AddWithValue("@Cardtype_ID", cardTypeId);
						command.Parameters.AddWithValue("@Expiry", expiry);
						command.Parameters.AddWithValue("@CARD_NUMBER", cardNumber);
						command.Parameters.AddWithValue("@CVV", cvv);

						// output-Parameter für Kundennummer hinzufügen
						SqlParameter output = new SqlParameter("@CHECK_PASSED", System.Data.SqlDbType.Bit);
						output.Direction = System.Data.ParameterDirection.Output;
						command.Parameters.Add(output);

						// ausführen
						command.ExecuteNonQuery();

						// bestätigung abholen
						bool passed = (bool)output.Value;

						return passed;
					}
				}
			}
			return false;
		}

		public bool UnlockCard(string customerName, string customerSurname, int cardTypeId, string expiry, string cardNumber, string cvv)
		{
			using (SqlConnection connection = GetConnection())
			{
				connection.Open();

				if (DoAuthorization(connection))
				{
					using (SqlCommand command = new SqlCommand("UnlockCard", connection) { CommandType = System.Data.CommandType.StoredProcedure })
					{
						// wir nehmen an, dass die Connection bereits geöffnet wurde!!!
						command.Parameters.AddWithValue("@Customer_Name", customerName);
						command.Parameters.AddWithValue("@Customer_Surname", customerSurname);
						command.Parameters.AddWithValue("@Cardtype_ID", cardTypeId);
						command.Parameters.AddWithValue("@Expiry", expiry);
						command.Parameters.AddWithValue("@CARD_NUMBER", cardNumber);
						command.Parameters.AddWithValue("@CVV", cvv);

						// output-Parameter für Kundennummer hinzufügen
						SqlParameter output = new SqlParameter("@UNLOCKED", System.Data.SqlDbType.Bit);
						output.Direction = System.Data.ParameterDirection.Output;
						command.Parameters.Add(output);

						// ausführen
						command.ExecuteNonQuery();

						// bestätigung abholen
						bool passed = (bool)output.Value;

						return passed;
					}
				}
			}
			return false;
		}

		#endregion

		#region Tests

		public bool AuthorizeTest()
		{
			using (SqlConnection connection = GetConnection())
			{
				connection.Open();

				bool ok = DoAuthorization(connection);

				connection.Close();

				return ok;
			}
		}

		public bool WriteTest()
		{
			using (SqlConnection connection = GetConnection())
			{
				connection.Open();

				bool ok = false;
				
				if(DoAuthorization(connection))
				{
					string queryString =	"INSERT INTO CardType (Cardtype_Name) " +
											"VALUES (@TypeName) ";
					try
					{
						using (SqlCommand command = new SqlCommand(queryString, connection))
						{
							command.Parameters.AddWithValue("@TypeName", "NurEinTestDerFehlschlagenSollte");
							int count = command.ExecuteNonQuery();
						}
					}
					catch (SqlException) // wir erwarten diese ausnahme, weil änderungen nach erfolgter Autorisierung nicht möglich sind
					{
						ok = true; // test wird nur bei auslösen der ausnahme als bestanden gewertet
					}

					return ok;
				}

				return ok;
			}
		}

		#endregion
	}
}
