﻿USE [master]
GO
/****** Object:  Database [Credit]    Script Date: 02.03.2016 14:43:45 ******/
CREATE DATABASE [Credit]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Credit', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Credit.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Credit_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Credit_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Credit] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Credit].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Credit] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Credit] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Credit] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Credit] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Credit] SET ARITHABORT OFF 
GO
ALTER DATABASE [Credit] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Credit] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Credit] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Credit] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Credit] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Credit] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Credit] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Credit] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Credit] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Credit] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Credit] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Credit] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Credit] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Credit] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Credit] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Credit] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Credit] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Credit] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Credit] SET  MULTI_USER 
GO
ALTER DATABASE [Credit] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Credit] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Credit] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Credit] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Credit] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Credit]
GO
/****** Object:  ApplicationRole [Readers]    Script Date: 02.03.2016 14:43:45 ******/
/* To avoid disclosure of passwords, the password is generated in script. */
declare @idx as int
declare @randomPwd as nvarchar(64)
declare @rnd as float
select @idx = 0
select @randomPwd = N''
select @rnd = rand((@@CPU_BUSY % 100) + ((@@IDLE % 100) * 100) + 
       (DATEPART(ss, GETDATE()) * 10000) + ((cast(DATEPART(ms, GETDATE()) as int) % 100) * 1000000))
while @idx < 64
begin
   select @randomPwd = @randomPwd + char((cast((@rnd * 83) as int) + 43))
   select @idx = @idx + 1
select @rnd = rand()
end
declare @statement nvarchar(4000)
select @statement = N'CREATE APPLICATION ROLE [Readers] WITH DEFAULT_SCHEMA = [dbo], ' + N'PASSWORD = N' + QUOTENAME(@randomPwd,'''')
EXEC dbo.sp_executesql @statement

GO
/****** Object:  Table [dbo].[Card]    Script Date: 02.03.2016 14:43:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Card](
	[Card_ID] [int] IDENTITY(1,1) NOT NULL,
	[Cardtype_ID] [int] NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Card_Number] [char](16) NOT NULL,
	[MonthOfExpiry] [int] NOT NULL,
	[YearOfExpiry] [int] NOT NULL,
	[CVV] [char](4) NOT NULL,
	[UnlockedAt] [datetime] NULL,
 CONSTRAINT [PK_Card] PRIMARY KEY CLUSTERED 
(
	[Card_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CardType]    Script Date: 02.03.2016 14:43:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardType](
	[Cardtype_ID] [int] IDENTITY(1,1) NOT NULL,
	[Cardtype_Name] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_CardType] PRIMARY KEY CLUSTERED 
(
	[Cardtype_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 02.03.2016 14:43:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Customer_ID] [int] IDENTITY(1,1) NOT NULL,
	[Customer_Name] [nvarchar](80) NOT NULL,
	[Customer_Surname] [nvarchar](80) NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Customer_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Log]    Script Date: 02.03.2016 14:43:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[Log_ID] [int] IDENTITY(1,1) NOT NULL,
	[Card_ID] [int] NOT NULL,
	[Granted] [bit] NOT NULL,
	[Generated] [datetime] NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[Log_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Card_Number]    Script Date: 02.03.2016 14:43:45 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Card_Number] ON [dbo].[Card]
(
	[Card_Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CardType_UniqueName]    Script Date: 02.03.2016 14:43:45 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_CardType_UniqueName] ON [dbo].[CardType]
(
	[Cardtype_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CustomerNameUnique]    Script Date: 02.03.2016 14:43:45 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_CustomerNameUnique] ON [dbo].[Customer]
(
	[Customer_Name] ASC,
	[Customer_Surname] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Card]  WITH CHECK ADD  CONSTRAINT [FK_Card_CardType] FOREIGN KEY([Cardtype_ID])
REFERENCES [dbo].[CardType] ([Cardtype_ID])
GO
ALTER TABLE [dbo].[Card] CHECK CONSTRAINT [FK_Card_CardType]
GO
ALTER TABLE [dbo].[Card]  WITH CHECK ADD  CONSTRAINT [FK_Card_Customer] FOREIGN KEY([Customer_ID])
REFERENCES [dbo].[Customer] ([Customer_ID])
GO
ALTER TABLE [dbo].[Card] CHECK CONSTRAINT [FK_Card_Customer]
GO
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Card] FOREIGN KEY([Card_ID])
REFERENCES [dbo].[Card] ([Card_ID])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Card]
GO
ALTER TABLE [dbo].[Card]  WITH CHECK ADD  CONSTRAINT [CK_CVV] CHECK  ((len([CVV])=(4) AND NOT [CVV] like '%[^0-9]%'))
GO
ALTER TABLE [dbo].[Card] CHECK CONSTRAINT [CK_CVV]
GO
ALTER TABLE [dbo].[Card]  WITH CHECK ADD  CONSTRAINT [CK_Month] CHECK  (([MonthOfExpiry]>(0) AND [MonthOfExpiry]<(13)))
GO
ALTER TABLE [dbo].[Card] CHECK CONSTRAINT [CK_Month]
GO
ALTER TABLE [dbo].[Card]  WITH CHECK ADD  CONSTRAINT [CK_Number] CHECK  ((len([Card_Number])=(16) AND NOT [Card_Number] like '%[^0-9]%'))
GO
ALTER TABLE [dbo].[Card] CHECK CONSTRAINT [CK_Number]
GO
ALTER TABLE [dbo].[Card]  WITH CHECK ADD  CONSTRAINT [CK_Year] CHECK  (([YearOfExpiry]>(0) AND [YearOfExpiry]<=(99)))
GO
ALTER TABLE [dbo].[Card] CHECK CONSTRAINT [CK_Year]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [CK_CustomerName] CHECK  ((len([Customer_Name])>(1)))
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [CK_CustomerName]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [CK_CustomerSurName] CHECK  ((len([Customer_Surname])>(1)))
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [CK_CustomerSurName]
GO
/****** Object:  StoredProcedure [dbo].[CheckCard]    Script Date: 02.03.2016 14:43:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Prüft eine Kreditkarte
-- =============================================
CREATE PROCEDURE [dbo].[CheckCard] 
	@Customer_Name varchar(80),
	@Customer_Surname varchar(80),
	@Cardtype_ID int,
	@Expiry char(5),
	@CARD_NUMBER char(16),
	@CVV char(4),
	@CHECK_PASSED bit OUTPUT
AS
BEGIN
	-- SQL-generiert
	SET NOCOUNT ON;

	DECLARE @CUSTOMER_COUNT int = -1
	DECLARE @CUSTOMER_ID int = -1

	DECLARE @CARDTYPE_COUNT int = -1

	DECLARE @MonthOfExpiry int = 100
	DECLARE @YearOfExpiry int = 100

	DECLARE @CARD_COUNT int = -1
	DECLARE @CARD_ID int = -1
	DECLARE @CARD_UNLOCKED_AT datetime = NULL

	DECLARE @CURRENT_YEAR int = 0
	DECLARE @CURRENT_MONTH int = 0
	DECLARE @CARD_EXPIRES_IN int = -1

	DECLARE @UNGRANTED_LOG_COUNT int = -1

    -- Eigentliche Arbeit
	
	-- 1. Validierung Kunde
	SET @CUSTOMER_COUNT =	CAST(
					(SELECT COUNT([Customer_ID])
					FROM [dbo].[Customer]
					WHERE Customer_Name = @Customer_Name AND Customer_Surname = @Customer_Surname) 
				AS int)
	IF @CUSTOMER_COUNT = 1
	BEGIN
		SET @CUSTOMER_ID =	CAST(
						(SELECT [Customer_ID]
						FROM [dbo].[Customer]
						WHERE Customer_Name = @Customer_Name AND Customer_Surname = @Customer_Surname) 
					AS int)
	END

	-- 2. Validierung Kartentyp
	SET @CARDTYPE_COUNT =	CAST(
					(SELECT COUNT([Cardtype_ID])
					FROM [dbo].[CardType]
					WHERE [Cardtype_ID] = @Cardtype_ID) 
				AS int)

	-- 3. Validierung Datum
	IF @Expiry IS NOT NULL AND LEN(@Expiry) = 5 AND SUBSTRING(@Expiry ,3 , 1) = '/'
	BEGIN
		
		-- Ablaufdatum als int hernehmen
		SET @MonthOfExpiry = CAST(SUBSTRING(@Expiry ,1 , 2) AS int)
		SET @YearOfExpiry = CAST(SUBSTRING(@Expiry ,4 , 2) AS int)
	END

	-- 4. Validierung Karte
	-- Kartennummer, Verbindung Kunde, Verbindung Kartentyp, CVV müssen geprüft werden
	SET @CARD_COUNT =	CAST(
					(SELECT COUNT([Card_ID])
					FROM [dbo].[Card]
					WHERE ([Cardtype_ID] = @Cardtype_ID AND [Customer_ID] = @CUSTOMER_ID AND [Card_Number] = @CARD_NUMBER AND [MonthOfExpiry] = @MonthOfExpiry AND [YearOfExpiry] = @YearOfExpiry AND [CVV] = @CVV)
					) 
				AS int)
	IF @CARD_COUNT = 1
	BEGIN
		SET @CARD_ID =	CAST(
						(SELECT [Card_ID]
						FROM [dbo].[Card]
						WHERE [Card_Number] = @CARD_NUMBER) 
					AS int)

		SET @CARD_UNLOCKED_AT =	CAST(
						(SELECT [UnlockedAt]
						FROM [dbo].[Card]
						WHERE [Card_Number] = @CARD_NUMBER) 
					AS int)
	END

	-- 5. Log-Einträge auf Sperrungen prüfen
	-- Variante 1: Karte wurde noch nie entsperrt
	IF @CARD_UNLOCKED_AT IS NULL
	BEGIN
	SET @UNGRANTED_LOG_COUNT =	CAST(
					(SELECT COUNT([Log_ID])
					FROM [dbo].[Log]
					WHERE [Card_ID] = @Card_ID AND [Granted] = 0) 
				AS int)
	END
	-- Variante 2: Karte wurde schon mal entsperrt - Abfrage muss entsprechend gefiltert werden
	ELSE
	BEGIN
		SET @UNGRANTED_LOG_COUNT =	CAST(
					(SELECT COUNT([Log_ID])
					FROM [dbo].[Log]
					WHERE [Card_ID] = @Card_ID AND [Granted] = 0 AND [Generated] > @CARD_UNLOCKED_AT) 
				AS int)
	END

	-- 6. Ablaufdatum gegen aktuelles Datum prüfen
	SET @CURRENT_YEAR = YEAR(GETDATE()) - 2000; -- (-2000, da wir mit zweistelligen jahren arbeiten)
	SET @CURRENT_MONTH = MONTH(GETDATE());

	SET @CARD_EXPIRES_IN = ((@YearOfExpiry - @CURRENT_YEAR) * 12) + @MonthOfExpiry - @CURRENT_MONTH

	-- 7. Auswerten, ob alle Validierungen und die Log-Einträge eine Freigabe erlauben
	IF @CUSTOMER_ID > 0 AND @CARDTYPE_COUNT = 1 AND @CARD_EXPIRES_IN >= 0 AND @CARD_ID > 0 AND @UNGRANTED_LOG_COUNT < 3
	BEGIN
		SET @CHECK_PASSED = 1
	END
	ELSE
	BEGIN
		SET @CHECK_PASSED = 0
	END

	-- 8. Entsprechenden Log-Eintrag erstellen
	IF @CARD_ID > 0 -- wird eigentlich nur aufgerufen, wenn alle Daten korrekt waren
	BEGIN
		INSERT INTO Log ([Card_ID], [Granted], [Generated]) VALUES(@CARD_ID, @CHECK_PASSED, GETDATE()) 
	END
	ELSE -- wird aufgerufen, wenn nicht alle Daten zur Karte korrekt waren, Karte soll ja auch so gesperrt werden können
	BEGIN
		SET @CARD_ID =	CAST(
						(SELECT [Card_ID]
						FROM [dbo].[Card]
						WHERE [Card_Number] = @CARD_NUMBER) 
					AS int)
		IF @CARD_ID > 0
			INSERT INTO Log ([Card_ID], [Granted], [Generated]) VALUES(@CARD_ID, @CHECK_PASSED, GETDATE()) 
		-- falls das nicht klappt war die komplette Kartennummer falsch und kann so auch nicht gesperrt werden
	END
END

GO
/****** Object:  StoredProcedure [dbo].[SaveCard]    Script Date: 02.03.2016 14:43:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Speichert eine neue Kreditkarte in die Datenbank
-- Generiert automatisch eine zufällige, 16-stellige Kreditkartennummer
-- Generiert automatisch eine zufällige, 4-stellige CVV-Nummber
-- Vailidierung des Datums:
-- a) Durch Cast auf int (nur Zahlen)
-- b) Durch CHECK-Constraints beim Insert
-- =============================================
CREATE PROCEDURE [dbo].[SaveCard]
	-- Parameter
	@Customer_ID int,
	@Cardtype_ID int,
	@Expiry char(5),
	@Card_ID int OUTPUT
AS
BEGIN
	-- SQL-Studio generiert
	SET NOCOUNT ON;

    -- Eigentliche Arbeit
	DECLARE @CUSTOMER_COUNT int
	DECLARE @CARDTYPE_COUNT int

	DECLARE @MonthOfExpiry int
	DECLARE @YearOfExpiry int

	-- Ablaufdatum grob validieren - Rest machen CHECKS
	-- SQL nutzt offenkundig einen 1-basierten index......
	IF @Expiry IS NOT NULL AND LEN(@Expiry) = 5 AND SUBSTRING(@Expiry ,3 , 1) = '/'
	BEGIN
		
		-- Ablaufdatum als int hernehmen
		SET @MonthOfExpiry = CAST(SUBSTRING(@Expiry ,1 , 2) AS int)
		SET @YearOfExpiry = CAST(SUBSTRING(@Expiry ,4 , 2) AS int)

		-- Zufallszahlen für Kartennummer und CVV generieren
		-- quelle zufallszahlen: http://stackoverflow.com/questions/7709042/generate-x-digit-random-number-in-sql-server-2008-r2
		DECLARE @CARD_NUMBER char(16) = convert(numeric(16,0),rand() * 8999999999999999) + 1000000000000000
		DECLARE @CVV char(4) = convert(numeric(4,0),rand() * 8999) + 1000

		-- gucken ob der Kunde wirklich existiert
		SET @CUSTOMER_COUNT =	CAST(
									(SELECT COUNT([Customer_ID])
									FROM [dbo].[Customer]
									WHERE Customer_ID = @Customer_ID) 
								AS int)
		IF @CUSTOMER_COUNT = 1 -- nur wenn kunde wirklich existiert
		BEGIN
			-- gucken, ob kartentyp wirklich existiert
			SET @CARDTYPE_COUNT =	CAST(
									(SELECT COUNT([Cardtype_ID])
									FROM [dbo].[CardType]
									WHERE [Cardtype_ID] = @Cardtype_ID) 
								AS int)
			IF @CARDTYPE_COUNT = 1 -- nur wenn kartentyp wirklich existiert
			BEGIN
				INSERT INTO Card (Cardtype_ID, Customer_ID, Card_Number, MonthOfExpiry, YearOfExpiry, CVV) VALUES (@Cardtype_ID, @Customer_ID, @CARD_NUMBER, @MonthOfExpiry, @YearOfExpiry, @CVV)
				SET @Card_ID = CAST((SELECT SCOPE_IDENTITY()) AS int);
			END
		END
	END
END


GO
/****** Object:  StoredProcedure [dbo].[SaveCustomer]    Script Date: 02.03.2016 14:43:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Gibt die ID eines zu findenden oder speichernden Kunden ab
-- Falls der Kunde noch nicht existiert - wird er gespeichert
-- Falls der Kunde existiert - wird dessen ID zurückgegeben
-- =============================================
CREATE PROCEDURE [dbo].[SaveCustomer]
	-- Parameter
	@Customer_Name varchar(80),
	@Customer_Surname varchar(80),
	@Customer_ID int OUTPUT
	
AS
BEGIN
	-- generiert von SQL-Studio
	SET NOCOUNT ON;

    -- eigentliche Arbeit
	
	DECLARE @COUNT int

	-- gucken ob der Kunde schon existiert
	SET @COUNT =	CAST(
					(SELECT COUNT([Customer_ID])
					FROM [dbo].[Customer]
					WHERE Customer_Name = @Customer_Name AND Customer_Surname = @Customer_Surname) 
				AS int)

	-- falls ja: id raussuchen und zurückgeben
	IF @COUNT = 1
		BEGIN
			SET @Customer_ID =	CAST(
							(SELECT [Customer_ID]
							FROM [dbo].[Customer]
							WHERE Customer_Name = @Customer_Name AND Customer_Surname = @Customer_Surname) 
						AS int)
		END
	-- falls nein: speichern, neue id holen und zurückgeben
	ELSE
		BEGIN
			INSERT INTO [dbo].[Customer] (Customer_Name, Customer_Surname) VALUES (@Customer_Name, @Customer_Surname)
			SET @Customer_ID = CAST((SELECT SCOPE_IDENTITY()) AS int);
		END
END



GO
/****** Object:  StoredProcedure [dbo].[UnlockCard]    Script Date: 02.03.2016 14:43:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Entsperrt eine Kreditkarte, welche durch mehrfache Eingabe fehlerhafter Daten gesperrt wurde
-- Entsperrung abgelaufener Karten o. ä. ist hiermit NICHT möglich
-- =============================================
CREATE PROCEDURE [dbo].[UnlockCard] 
	-- Add the parameters for the stored procedure here
	@Customer_Name varchar(80),
	@Customer_Surname varchar(80),
	@Cardtype_ID int,
	@Expiry char(5),
	@CARD_NUMBER char(16),
	@CVV char(4),
	@UNLOCKED bit OUTPUT
AS
BEGIN
	-- SQL-generiert
	SET NOCOUNT ON;

    DECLARE @CUSTOMER_COUNT int = -1
	DECLARE @CUSTOMER_ID int = -1

	DECLARE @CARDTYPE_COUNT int = -1

	DECLARE @MonthOfExpiry int = 100
	DECLARE @YearOfExpiry int = 100

	DECLARE @CARD_COUNT int = -1
	DECLARE @CARD_ID int = -1

	DECLARE @CURRENT_YEAR int = 0
	DECLARE @CURRENT_MONTH int = 0
	DECLARE @CARD_EXPIRES_IN int = -1

	DECLARE @UNGRANTED_LOG_COUNT int = -1

    -- Eigentliche Arbeit
	
	-- 1. Validierung Kunde
	SET @CUSTOMER_COUNT =	CAST(
					(SELECT COUNT([Customer_ID])
					FROM [dbo].[Customer]
					WHERE Customer_Name = @Customer_Name AND Customer_Surname = @Customer_Surname) 
				AS int)
	IF @CUSTOMER_COUNT = 1
	BEGIN
		SET @CUSTOMER_ID =	CAST(
						(SELECT [Customer_ID]
						FROM [dbo].[Customer]
						WHERE Customer_Name = @Customer_Name AND Customer_Surname = @Customer_Surname) 
					AS int)
	END

	-- 2. Validierung Kartentyp
	SET @CARDTYPE_COUNT =	CAST(
					(SELECT COUNT([Cardtype_ID])
					FROM [dbo].[CardType]
					WHERE [Cardtype_ID] = @Cardtype_ID) 
				AS int)

	-- 3. Validierung Datum
	IF @Expiry IS NOT NULL AND LEN(@Expiry) = 5 AND SUBSTRING(@Expiry ,3 , 1) = '/'
	BEGIN
		
		-- Ablaufdatum als int hernehmen
		SET @MonthOfExpiry = CAST(SUBSTRING(@Expiry ,1 , 2) AS int)
		SET @YearOfExpiry = CAST(SUBSTRING(@Expiry ,4 , 2) AS int)
	END

	-- 4. Validierung Karte
	-- Kartennummer, Verbindung Kunde, Verbindung Kartentyp, CVV müssen geprüft werden
	SET @CARD_COUNT =	CAST(
					(SELECT COUNT([Card_ID])
					FROM [dbo].[Card]
					WHERE ([Cardtype_ID] = @Cardtype_ID AND [Customer_ID] = @CUSTOMER_ID AND [Card_Number] = @CARD_NUMBER AND [MonthOfExpiry] = @MonthOfExpiry AND [YearOfExpiry] = @YearOfExpiry AND [CVV] = @CVV)
					) 
				AS int)
	IF @CARD_COUNT = 1
	BEGIN
		SET @CARD_ID =	CAST(
						(SELECT [Card_ID]
						FROM [dbo].[Card]
						WHERE [Card_Number] = @CARD_NUMBER) 
					AS int)
	END

	-- 5. Ablaufdatum gegen aktuelles Datum prüfen
	SET @CURRENT_YEAR = YEAR(GETDATE()) - 2000; -- (-2000, da wir mit zweistelligen jahren arbeiten)
	SET @CURRENT_MONTH = MONTH(GETDATE());

	SET @CARD_EXPIRES_IN = ((@YearOfExpiry - @CURRENT_YEAR) * 12) + @MonthOfExpiry - @CURRENT_MONTH

	-- 6. Auswerten, ob alle Validierungen und die Log-Einträge eine Freigabe erlauben
	IF @CUSTOMER_ID > 0 AND @CARDTYPE_COUNT = 1 AND @CARD_EXPIRES_IN >= 0 AND @CARD_ID > 0 AND @UNGRANTED_LOG_COUNT < 3
	BEGIN
		SET @UNLOCKED = 1
	END
	ELSE
	BEGIN
		SET @UNLOCKED = 0
	END

	-- 7. Karte entsperren
	IF @UNLOCKED = 1 -- wird nur aufgerufen, wenn alle Daten korrekt waren
	BEGIN
		UPDATE [dbo].[Card]
		   SET [UnlockedAt] = GETDATE()
		 WHERE [Card_ID] = @CARD_ID
	END
END

GO
USE [master]
GO
ALTER DATABASE [Credit] SET  READ_WRITE 
GO
