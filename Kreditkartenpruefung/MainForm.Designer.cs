﻿namespace Kreditkartenpruefung
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.BoxCheckCreditcard = new System.Windows.Forms.GroupBox();
			this.result = new System.Windows.Forms.RichTextBox();
			this.checkCreditcard = new System.Windows.Forms.Button();
			this.CustomerEnrtry = new System.Windows.Forms.GroupBox();
			this.CustomerName = new System.Windows.Forms.TextBox();
			this.CustomerSurname = new System.Windows.Forms.TextBox();
			this.customerNameText = new System.Windows.Forms.Label();
			this.customerSurnameText = new System.Windows.Forms.Label();
			this.chooseCreditcard = new System.Windows.Forms.GroupBox();
			this.CVV = new System.Windows.Forms.TextBox();
			this.CardNumber = new System.Windows.Forms.TextBox();
			this.DateOfExpiry = new System.Windows.Forms.TextBox();
			this.CVVText = new System.Windows.Forms.Label();
			this.CardnumberText = new System.Windows.Forms.Label();
			this.DateOfExpiryText = new System.Windows.Forms.Label();
			this.visacard = new System.Windows.Forms.PictureBox();
			this.dinersclub = new System.Windows.Forms.PictureBox();
			this.maestro = new System.Windows.Forms.PictureBox();
			this.mastercard = new System.Windows.Forms.PictureBox();
			this.isMaestro = new System.Windows.Forms.RadioButton();
			this.isDinersClub = new System.Windows.Forms.RadioButton();
			this.isVisa = new System.Windows.Forms.RadioButton();
			this.isMasterCard = new System.Windows.Forms.RadioButton();
			this.BoxNewCreditcard = new System.Windows.Forms.GroupBox();
			this.resultCreateCreditcard = new System.Windows.Forms.RichTextBox();
			this.makeNewCreditcard = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.automCVV = new System.Windows.Forms.TextBox();
			this.automCardNumber = new System.Windows.Forms.TextBox();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.visacard2 = new System.Windows.Forms.PictureBox();
			this.dinersclub2 = new System.Windows.Forms.PictureBox();
			this.maestro2 = new System.Windows.Forms.PictureBox();
			this.mastercard2 = new System.Windows.Forms.PictureBox();
			this.newMaestroCard = new System.Windows.Forms.RadioButton();
			this.NewDinersClub = new System.Windows.Forms.RadioButton();
			this.newVisaCard = new System.Windows.Forms.RadioButton();
			this.newMasterCard = new System.Windows.Forms.RadioButton();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.NewCustomerName = new System.Windows.Forms.TextBox();
			this.NewCustomerSurname = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.richTextBox2 = new System.Windows.Forms.RichTextBox();
			this.LogProtokoll = new System.Windows.Forms.GroupBox();
			this.myApplication = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.BoxCheckCreditcard.SuspendLayout();
			this.CustomerEnrtry.SuspendLayout();
			this.chooseCreditcard.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.visacard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dinersclub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.maestro)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mastercard)).BeginInit();
			this.BoxNewCreditcard.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.visacard2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dinersclub2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.maestro2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mastercard2)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.LogProtokoll.SuspendLayout();
			this.SuspendLayout();
			// 
			// BoxCheckCreditcard
			// 
			this.BoxCheckCreditcard.BackColor = System.Drawing.SystemColors.ControlDark;
			this.BoxCheckCreditcard.Controls.Add(this.button1);
			this.BoxCheckCreditcard.Controls.Add(this.result);
			this.BoxCheckCreditcard.Controls.Add(this.checkCreditcard);
			this.BoxCheckCreditcard.Controls.Add(this.CustomerEnrtry);
			this.BoxCheckCreditcard.Controls.Add(this.chooseCreditcard);
			this.BoxCheckCreditcard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BoxCheckCreditcard.Location = new System.Drawing.Point(22, 21);
			this.BoxCheckCreditcard.Name = "BoxCheckCreditcard";
			this.BoxCheckCreditcard.Size = new System.Drawing.Size(471, 410);
			this.BoxCheckCreditcard.TabIndex = 0;
			this.BoxCheckCreditcard.TabStop = false;
			this.BoxCheckCreditcard.Text = "Überprüfung einer Kreditkarte";
			// 
			// result
			// 
			this.result.BackColor = System.Drawing.SystemColors.ControlDark;
			this.result.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.result.Location = new System.Drawing.Point(162, 360);
			this.result.Name = "result";
			this.result.ReadOnly = true;
			this.result.Size = new System.Drawing.Size(275, 22);
			this.result.TabIndex = 3;
			this.result.Text = "";
			// 
			// checkCreditcard
			// 
			this.checkCreditcard.Location = new System.Drawing.Point(16, 360);
			this.checkCreditcard.Name = "checkCreditcard";
			this.checkCreditcard.Size = new System.Drawing.Size(137, 23);
			this.checkCreditcard.TabIndex = 2;
			this.checkCreditcard.Text = "Kreditkarte überprüfen";
			this.checkCreditcard.UseVisualStyleBackColor = true;
			this.checkCreditcard.Click += new System.EventHandler(this.checkCreditcard_Click);
			// 
			// CustomerEnrtry
			// 
			this.CustomerEnrtry.Controls.Add(this.CustomerName);
			this.CustomerEnrtry.Controls.Add(this.CustomerSurname);
			this.CustomerEnrtry.Controls.Add(this.customerNameText);
			this.CustomerEnrtry.Controls.Add(this.customerSurnameText);
			this.CustomerEnrtry.Location = new System.Drawing.Point(16, 246);
			this.CustomerEnrtry.Name = "CustomerEnrtry";
			this.CustomerEnrtry.Size = new System.Drawing.Size(440, 91);
			this.CustomerEnrtry.TabIndex = 1;
			this.CustomerEnrtry.TabStop = false;
			this.CustomerEnrtry.Text = "Angaben des Kunden";
			// 
			// CustomerName
			// 
			this.CustomerName.Location = new System.Drawing.Point(146, 58);
			this.CustomerName.Name = "CustomerName";
			this.CustomerName.Size = new System.Drawing.Size(275, 20);
			this.CustomerName.TabIndex = 14;
			// 
			// CustomerSurname
			// 
			this.CustomerSurname.Location = new System.Drawing.Point(146, 30);
			this.CustomerSurname.Name = "CustomerSurname";
			this.CustomerSurname.Size = new System.Drawing.Size(275, 20);
			this.CustomerSurname.TabIndex = 13;
			// 
			// customerNameText
			// 
			this.customerNameText.AutoSize = true;
			this.customerNameText.Location = new System.Drawing.Point(25, 61);
			this.customerNameText.Name = "customerNameText";
			this.customerNameText.Size = new System.Drawing.Size(83, 13);
			this.customerNameText.TabIndex = 1;
			this.customerNameText.Text = "Vorname Kunde";
			// 
			// customerSurnameText
			// 
			this.customerSurnameText.AutoSize = true;
			this.customerSurnameText.Location = new System.Drawing.Point(25, 33);
			this.customerSurnameText.Name = "customerSurnameText";
			this.customerSurnameText.Size = new System.Drawing.Size(93, 13);
			this.customerSurnameText.TabIndex = 0;
			this.customerSurnameText.Text = "Nachname Kunde";
			// 
			// chooseCreditcard
			// 
			this.chooseCreditcard.Controls.Add(this.CVV);
			this.chooseCreditcard.Controls.Add(this.CardNumber);
			this.chooseCreditcard.Controls.Add(this.DateOfExpiry);
			this.chooseCreditcard.Controls.Add(this.CVVText);
			this.chooseCreditcard.Controls.Add(this.CardnumberText);
			this.chooseCreditcard.Controls.Add(this.DateOfExpiryText);
			this.chooseCreditcard.Controls.Add(this.visacard);
			this.chooseCreditcard.Controls.Add(this.dinersclub);
			this.chooseCreditcard.Controls.Add(this.maestro);
			this.chooseCreditcard.Controls.Add(this.mastercard);
			this.chooseCreditcard.Controls.Add(this.isMaestro);
			this.chooseCreditcard.Controls.Add(this.isDinersClub);
			this.chooseCreditcard.Controls.Add(this.isVisa);
			this.chooseCreditcard.Controls.Add(this.isMasterCard);
			this.chooseCreditcard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.chooseCreditcard.Location = new System.Drawing.Point(16, 20);
			this.chooseCreditcard.Name = "chooseCreditcard";
			this.chooseCreditcard.Size = new System.Drawing.Size(440, 209);
			this.chooseCreditcard.TabIndex = 0;
			this.chooseCreditcard.TabStop = false;
			this.chooseCreditcard.Text = "Auswahl der Kreditkarte";
			// 
			// CVV
			// 
			this.CVV.Location = new System.Drawing.Point(146, 169);
			this.CVV.Name = "CVV";
			this.CVV.Size = new System.Drawing.Size(275, 20);
			this.CVV.TabIndex = 12;
			// 
			// CardNumber
			// 
			this.CardNumber.Location = new System.Drawing.Point(146, 143);
			this.CardNumber.Name = "CardNumber";
			this.CardNumber.Size = new System.Drawing.Size(275, 20);
			this.CardNumber.TabIndex = 11;
			// 
			// DateOfExpiry
			// 
			this.DateOfExpiry.Location = new System.Drawing.Point(146, 117);
			this.DateOfExpiry.Name = "DateOfExpiry";
			this.DateOfExpiry.Size = new System.Drawing.Size(275, 20);
			this.DateOfExpiry.TabIndex = 10;
			// 
			// CVVText
			// 
			this.CVVText.AutoSize = true;
			this.CVVText.Location = new System.Drawing.Point(22, 172);
			this.CVVText.Name = "CVVText";
			this.CVVText.Size = new System.Drawing.Size(28, 13);
			this.CVVText.TabIndex = 9;
			this.CVVText.Text = "CVV";
			// 
			// CardnumberText
			// 
			this.CardnumberText.AutoSize = true;
			this.CardnumberText.Location = new System.Drawing.Point(22, 146);
			this.CardnumberText.Name = "CardnumberText";
			this.CardnumberText.Size = new System.Drawing.Size(55, 13);
			this.CardnumberText.TabIndex = 8;
			this.CardnumberText.Text = "Karten Nr.";
			// 
			// DateOfExpiryText
			// 
			this.DateOfExpiryText.AutoSize = true;
			this.DateOfExpiryText.Location = new System.Drawing.Point(22, 120);
			this.DateOfExpiryText.Name = "DateOfExpiryText";
			this.DateOfExpiryText.Size = new System.Drawing.Size(90, 13);
			this.DateOfExpiryText.TabIndex = 7;
			this.DateOfExpiryText.Text = "Gültig bis MM/YY";
			// 
			// visacard
			// 
			this.visacard.Image = global::Kreditkartenpruefung.Properties.Resources.visaCard;
			this.visacard.Location = new System.Drawing.Point(131, 27);
			this.visacard.Name = "visacard";
			this.visacard.Size = new System.Drawing.Size(63, 33);
			this.visacard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.visacard.TabIndex = 4;
			this.visacard.TabStop = false;
			// 
			// dinersclub
			// 
			this.dinersclub.Image = global::Kreditkartenpruefung.Properties.Resources.dinersCard;
			this.dinersclub.Location = new System.Drawing.Point(240, 27);
			this.dinersclub.Name = "dinersclub";
			this.dinersclub.Size = new System.Drawing.Size(63, 33);
			this.dinersclub.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.dinersclub.TabIndex = 5;
			this.dinersclub.TabStop = false;
			// 
			// maestro
			// 
			this.maestro.Image = global::Kreditkartenpruefung.Properties.Resources.maestroCard;
			this.maestro.Location = new System.Drawing.Point(358, 27);
			this.maestro.Name = "maestro";
			this.maestro.Size = new System.Drawing.Size(63, 33);
			this.maestro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.maestro.TabIndex = 6;
			this.maestro.TabStop = false;
			// 
			// mastercard
			// 
			this.mastercard.Image = global::Kreditkartenpruefung.Properties.Resources.masterCard;
			this.mastercard.Location = new System.Drawing.Point(25, 28);
			this.mastercard.Name = "mastercard";
			this.mastercard.Size = new System.Drawing.Size(63, 33);
			this.mastercard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.mastercard.TabIndex = 1;
			this.mastercard.TabStop = false;
			// 
			// isMaestro
			// 
			this.isMaestro.AutoSize = true;
			this.isMaestro.Location = new System.Drawing.Point(358, 71);
			this.isMaestro.Name = "isMaestro";
			this.isMaestro.Size = new System.Drawing.Size(63, 17);
			this.isMaestro.TabIndex = 3;
			this.isMaestro.TabStop = true;
			this.isMaestro.Text = "Maestro";
			this.isMaestro.UseVisualStyleBackColor = true;
			// 
			// isDinersClub
			// 
			this.isDinersClub.AutoSize = true;
			this.isDinersClub.Location = new System.Drawing.Point(240, 72);
			this.isDinersClub.Name = "isDinersClub";
			this.isDinersClub.Size = new System.Drawing.Size(79, 17);
			this.isDinersClub.TabIndex = 2;
			this.isDinersClub.TabStop = true;
			this.isDinersClub.Text = "Diners Club";
			this.isDinersClub.UseVisualStyleBackColor = true;
			// 
			// isVisa
			// 
			this.isVisa.AutoSize = true;
			this.isVisa.Location = new System.Drawing.Point(131, 71);
			this.isVisa.Name = "isVisa";
			this.isVisa.Size = new System.Drawing.Size(45, 17);
			this.isVisa.TabIndex = 1;
			this.isVisa.TabStop = true;
			this.isVisa.Text = "Visa";
			this.isVisa.UseVisualStyleBackColor = true;
			// 
			// isMasterCard
			// 
			this.isMasterCard.AutoSize = true;
			this.isMasterCard.Location = new System.Drawing.Point(25, 71);
			this.isMasterCard.Name = "isMasterCard";
			this.isMasterCard.Size = new System.Drawing.Size(82, 17);
			this.isMasterCard.TabIndex = 0;
			this.isMasterCard.TabStop = true;
			this.isMasterCard.Text = "Master Card";
			this.isMasterCard.UseVisualStyleBackColor = true;
			// 
			// BoxNewCreditcard
			// 
			this.BoxNewCreditcard.BackColor = System.Drawing.SystemColors.ControlDark;
			this.BoxNewCreditcard.Controls.Add(this.resultCreateCreditcard);
			this.BoxNewCreditcard.Controls.Add(this.makeNewCreditcard);
			this.BoxNewCreditcard.Controls.Add(this.groupBox3);
			this.BoxNewCreditcard.Controls.Add(this.groupBox2);
			this.BoxNewCreditcard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BoxNewCreditcard.Location = new System.Drawing.Point(524, 21);
			this.BoxNewCreditcard.Name = "BoxNewCreditcard";
			this.BoxNewCreditcard.Size = new System.Drawing.Size(471, 410);
			this.BoxNewCreditcard.TabIndex = 4;
			this.BoxNewCreditcard.TabStop = false;
			this.BoxNewCreditcard.Text = "Neue Kreditcarte anlegen";
			// 
			// resultCreateCreditcard
			// 
			this.resultCreateCreditcard.BackColor = System.Drawing.SystemColors.ControlDark;
			this.resultCreateCreditcard.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.resultCreateCreditcard.Location = new System.Drawing.Point(162, 360);
			this.resultCreateCreditcard.Name = "resultCreateCreditcard";
			this.resultCreateCreditcard.ReadOnly = true;
			this.resultCreateCreditcard.Size = new System.Drawing.Size(275, 22);
			this.resultCreateCreditcard.TabIndex = 3;
			this.resultCreateCreditcard.Text = "";
			// 
			// makeNewCreditcard
			// 
			this.makeNewCreditcard.Location = new System.Drawing.Point(16, 360);
			this.makeNewCreditcard.Name = "makeNewCreditcard";
			this.makeNewCreditcard.Size = new System.Drawing.Size(137, 23);
			this.makeNewCreditcard.TabIndex = 2;
			this.makeNewCreditcard.Text = "Kreditkarte erstellen";
			this.makeNewCreditcard.UseVisualStyleBackColor = true;
			this.makeNewCreditcard.Click += new System.EventHandler(this.makeNewCreditcard_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.automCVV);
			this.groupBox3.Controls.Add(this.automCardNumber);
			this.groupBox3.Controls.Add(this.textBox5);
			this.groupBox3.Controls.Add(this.label3);
			this.groupBox3.Controls.Add(this.label4);
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.Controls.Add(this.visacard2);
			this.groupBox3.Controls.Add(this.dinersclub2);
			this.groupBox3.Controls.Add(this.maestro2);
			this.groupBox3.Controls.Add(this.mastercard2);
			this.groupBox3.Controls.Add(this.newMaestroCard);
			this.groupBox3.Controls.Add(this.NewDinersClub);
			this.groupBox3.Controls.Add(this.newVisaCard);
			this.groupBox3.Controls.Add(this.newMasterCard);
			this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox3.Location = new System.Drawing.Point(16, 117);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(440, 209);
			this.groupBox3.TabIndex = 0;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Auswahl der Kreditkarte";
			// 
			// automCVV
			// 
			this.automCVV.Location = new System.Drawing.Point(146, 169);
			this.automCVV.Name = "automCVV";
			this.automCVV.ReadOnly = true;
			this.automCVV.Size = new System.Drawing.Size(275, 20);
			this.automCVV.TabIndex = 14;
			// 
			// automCardNumber
			// 
			this.automCardNumber.Location = new System.Drawing.Point(146, 143);
			this.automCardNumber.Name = "automCardNumber";
			this.automCardNumber.ReadOnly = true;
			this.automCardNumber.Size = new System.Drawing.Size(275, 20);
			this.automCardNumber.TabIndex = 13;
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(146, 117);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(275, 20);
			this.textBox5.TabIndex = 10;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(22, 172);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(28, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "CVV";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(22, 146);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(61, 13);
			this.label4.TabIndex = 8;
			this.label4.Text = "Kartnen Nr.";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(22, 120);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(90, 13);
			this.label5.TabIndex = 7;
			this.label5.Text = "Gültig bis MM/YY";
			// 
			// visacard2
			// 
			this.visacard2.Image = global::Kreditkartenpruefung.Properties.Resources.visaCard;
			this.visacard2.Location = new System.Drawing.Point(131, 27);
			this.visacard2.Name = "visacard2";
			this.visacard2.Size = new System.Drawing.Size(63, 33);
			this.visacard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.visacard2.TabIndex = 4;
			this.visacard2.TabStop = false;
			// 
			// dinersclub2
			// 
			this.dinersclub2.Image = global::Kreditkartenpruefung.Properties.Resources.dinersCard;
			this.dinersclub2.Location = new System.Drawing.Point(240, 27);
			this.dinersclub2.Name = "dinersclub2";
			this.dinersclub2.Size = new System.Drawing.Size(63, 33);
			this.dinersclub2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.dinersclub2.TabIndex = 5;
			this.dinersclub2.TabStop = false;
			// 
			// maestro2
			// 
			this.maestro2.Image = global::Kreditkartenpruefung.Properties.Resources.maestroCard;
			this.maestro2.Location = new System.Drawing.Point(358, 27);
			this.maestro2.Name = "maestro2";
			this.maestro2.Size = new System.Drawing.Size(63, 33);
			this.maestro2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.maestro2.TabIndex = 6;
			this.maestro2.TabStop = false;
			// 
			// mastercard2
			// 
			this.mastercard2.Image = global::Kreditkartenpruefung.Properties.Resources.masterCard;
			this.mastercard2.Location = new System.Drawing.Point(25, 28);
			this.mastercard2.Name = "mastercard2";
			this.mastercard2.Size = new System.Drawing.Size(63, 33);
			this.mastercard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.mastercard2.TabIndex = 1;
			this.mastercard2.TabStop = false;
			// 
			// newMaestroCard
			// 
			this.newMaestroCard.AutoSize = true;
			this.newMaestroCard.Location = new System.Drawing.Point(358, 71);
			this.newMaestroCard.Name = "newMaestroCard";
			this.newMaestroCard.Size = new System.Drawing.Size(63, 17);
			this.newMaestroCard.TabIndex = 3;
			this.newMaestroCard.TabStop = true;
			this.newMaestroCard.Text = "Maestro";
			this.newMaestroCard.UseVisualStyleBackColor = true;
			// 
			// NewDinersClub
			// 
			this.NewDinersClub.AutoSize = true;
			this.NewDinersClub.Location = new System.Drawing.Point(240, 72);
			this.NewDinersClub.Name = "NewDinersClub";
			this.NewDinersClub.Size = new System.Drawing.Size(79, 17);
			this.NewDinersClub.TabIndex = 2;
			this.NewDinersClub.TabStop = true;
			this.NewDinersClub.Text = "Diners Club";
			this.NewDinersClub.UseVisualStyleBackColor = true;
			// 
			// newVisaCard
			// 
			this.newVisaCard.AutoSize = true;
			this.newVisaCard.Location = new System.Drawing.Point(131, 71);
			this.newVisaCard.Name = "newVisaCard";
			this.newVisaCard.Size = new System.Drawing.Size(45, 17);
			this.newVisaCard.TabIndex = 1;
			this.newVisaCard.TabStop = true;
			this.newVisaCard.Text = "Visa";
			this.newVisaCard.UseVisualStyleBackColor = true;
			// 
			// newMasterCard
			// 
			this.newMasterCard.AutoSize = true;
			this.newMasterCard.Location = new System.Drawing.Point(25, 71);
			this.newMasterCard.Name = "newMasterCard";
			this.newMasterCard.Size = new System.Drawing.Size(82, 17);
			this.newMasterCard.TabIndex = 0;
			this.newMasterCard.TabStop = true;
			this.newMasterCard.Text = "Master Card";
			this.newMasterCard.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.NewCustomerName);
			this.groupBox2.Controls.Add(this.NewCustomerSurname);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Location = new System.Drawing.Point(16, 20);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(440, 91);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Angaben des Kunden";
			// 
			// NewCustomerName
			// 
			this.NewCustomerName.Location = new System.Drawing.Point(146, 58);
			this.NewCustomerName.Name = "NewCustomerName";
			this.NewCustomerName.Size = new System.Drawing.Size(275, 20);
			this.NewCustomerName.TabIndex = 14;
			// 
			// NewCustomerSurname
			// 
			this.NewCustomerSurname.Location = new System.Drawing.Point(146, 30);
			this.NewCustomerSurname.Name = "NewCustomerSurname";
			this.NewCustomerSurname.Size = new System.Drawing.Size(275, 20);
			this.NewCustomerSurname.TabIndex = 13;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(25, 61);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(83, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Vorname Kunde";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(25, 33);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(93, 13);
			this.label2.TabIndex = 0;
			this.label2.Text = "Nachname Kunde";
			// 
			// richTextBox2
			// 
			this.richTextBox2.Location = new System.Drawing.Point(6, 16);
			this.richTextBox2.Name = "richTextBox2";
			this.richTextBox2.ReadOnly = true;
			this.richTextBox2.Size = new System.Drawing.Size(961, 75);
			this.richTextBox2.TabIndex = 4;
			this.richTextBox2.Text = "";
			// 
			// LogProtokoll
			// 
			this.LogProtokoll.Controls.Add(this.richTextBox2);
			this.LogProtokoll.Location = new System.Drawing.Point(22, 447);
			this.LogProtokoll.Name = "LogProtokoll";
			this.LogProtokoll.Size = new System.Drawing.Size(973, 100);
			this.LogProtokoll.TabIndex = 5;
			this.LogProtokoll.TabStop = false;
			this.LogProtokoll.Text = "Protokoll";
			// 
			// myApplication
			// 
			this.myApplication.BackColor = System.Drawing.SystemColors.ControlDark;
			this.myApplication.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.myApplication.Location = new System.Drawing.Point(791, 607);
			this.myApplication.Name = "myApplication";
			this.myApplication.ReadOnly = true;
			this.myApplication.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.myApplication.Size = new System.Drawing.Size(207, 13);
			this.myApplication.TabIndex = 6;
			this.myApplication.Text = "Kreditkarten Zugangsprüfung - Nicole Graf";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(339, 374);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 4;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlDark;
			this.ClientSize = new System.Drawing.Size(1022, 632);
			this.Controls.Add(this.myApplication);
			this.Controls.Add(this.LogProtokoll);
			this.Controls.Add(this.BoxNewCreditcard);
			this.Controls.Add(this.BoxCheckCreditcard);
			this.Name = "MainForm";
			this.Text = "Hauptform";
			this.BoxCheckCreditcard.ResumeLayout(false);
			this.CustomerEnrtry.ResumeLayout(false);
			this.CustomerEnrtry.PerformLayout();
			this.chooseCreditcard.ResumeLayout(false);
			this.chooseCreditcard.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.visacard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dinersclub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.maestro)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mastercard)).EndInit();
			this.BoxNewCreditcard.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.visacard2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dinersclub2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.maestro2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mastercard2)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.LogProtokoll.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox BoxCheckCreditcard;
        private System.Windows.Forms.GroupBox chooseCreditcard;
        private System.Windows.Forms.RadioButton isMaestro;
        private System.Windows.Forms.RadioButton isDinersClub;
        private System.Windows.Forms.RadioButton isVisa;
        private System.Windows.Forms.RadioButton isMasterCard;
        private System.Windows.Forms.PictureBox visacard;
        private System.Windows.Forms.PictureBox dinersclub;
        private System.Windows.Forms.PictureBox maestro;
        private System.Windows.Forms.PictureBox mastercard;
        private System.Windows.Forms.Label CardnumberText;
        private System.Windows.Forms.Label DateOfExpiryText;
        private System.Windows.Forms.TextBox CardNumber;
        private System.Windows.Forms.TextBox DateOfExpiry;
        private System.Windows.Forms.Label CVVText;
        private System.Windows.Forms.TextBox CVV;
        private System.Windows.Forms.RichTextBox result;
        private System.Windows.Forms.Button checkCreditcard;
        private System.Windows.Forms.GroupBox CustomerEnrtry;
        private System.Windows.Forms.TextBox CustomerName;
        private System.Windows.Forms.TextBox CustomerSurname;
        private System.Windows.Forms.Label customerNameText;
        private System.Windows.Forms.Label customerSurnameText;
        private System.Windows.Forms.GroupBox BoxNewCreditcard;
        private System.Windows.Forms.RichTextBox resultCreateCreditcard;
        private System.Windows.Forms.Button makeNewCreditcard;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox NewCustomerName;
        private System.Windows.Forms.TextBox NewCustomerSurname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox visacard2;
        private System.Windows.Forms.PictureBox dinersclub2;
        private System.Windows.Forms.PictureBox maestro2;
        private System.Windows.Forms.PictureBox mastercard2;
        private System.Windows.Forms.RadioButton newMaestroCard;
        private System.Windows.Forms.RadioButton NewDinersClub;
        private System.Windows.Forms.RadioButton newVisaCard;
        private System.Windows.Forms.RadioButton newMasterCard;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.GroupBox LogProtokoll;
        private System.Windows.Forms.TextBox myApplication;
		private System.Windows.Forms.TextBox automCVV;
		private System.Windows.Forms.TextBox automCardNumber;
		private System.Windows.Forms.Button button1;
	}
}

