﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Kreditkartenpruefung.Data;

namespace Kreditkartenpruefung
{
	public partial class MainForm : Form
    {
		int erstellenKartenTyp = -1;
		int pruefenKartenTyp = -1;

        public MainForm()
        {
            InitializeComponent();
			LoadCreditCardTypes();
		}

		private void checkCreditcard_Click(object sender, EventArgs e)
		{
			if (pruefenKartenTyp != -1)
			{
				DatenbankHelfer db = new DatenbankHelfer();
				if (db.CheckCard(CustomerName.Text, CustomerSurname.Text, pruefenKartenTyp, DateOfExpiry.Text, CardNumber.Text, CVV.Text))
				{
					ReportSuccess("Die Prüfung der Kreditkarte war erfolgreich!", CheckResult);
				}
				else
				{
					ReportFault("Die Prüfung der Kreditkarte war nicht erfolgreich!", CheckResult);
				}
			}
		}

		private void makeNewCreditcard_Click(object sender, EventArgs e)
		{
			if (erstellenKartenTyp != -1)
			{
				DatenbankHelfer db = new DatenbankHelfer();
				var details = db.SaveNewCard(NewCustomerName.Text, NewCustomerSurname.Text, erstellenKartenTyp, NewExpiry.Text);
				if (details != null)
				{
					automCardNumber.Text = details.Number;
					automCVV.Text = details.CVV;
					ReportSuccess("Die Eintragung der Kreditkarte war erfolgreich!", NewResult);
				}
				else
				{
					automCardNumber.Text = "";
					automCVV.Text = "";
					ReportFault("Hier ist beim Speichern etwas schief gegangen - bitte prüfen Sie die übergebenen Daten!", NewResult);
				}
			}
		}

		private void unlockCard_Click(object sender, EventArgs e)
		{
			if (pruefenKartenTyp != -1)
			{
				DatenbankHelfer db = new DatenbankHelfer();
				if (db.UnlockCard(CustomerName.Text, CustomerSurname.Text, pruefenKartenTyp, DateOfExpiry.Text, CardNumber.Text, CVV.Text))
				{
					ReportSuccess("Die Entsperrung der Kreditkarte war erfolgreich!", UnlockResult);
				}
				else
				{
					ReportFault("Die Entsperrung der Kreditkarte war nicht erfolgreich!", UnlockResult);
				}
			}
		}

		private void LoadCreditCardTypes()
		{
			DatenbankHelfer db = new DatenbankHelfer();
			List<CardType> types = db.GetCardTypes();

			// panel 1
			foreach (CardType type in types)
			{
				RadioButton rb = new RadioButton();
				rb.Tag = type; // Kartentyp im Radiobutton speichern
				rb.Text = type.Name;
				//rb.Image = Image.FromFile(type.PicturePath);
				rb.TextImageRelation = TextImageRelation.ImageAboveText;
				rb.Height = 70;
				rb.Width = 90;

				if (type.ID == 1)
				{
					rb.Checked = true;
					pruefenKartenTyp = type.ID;
				}

				rb.CheckedChanged += Rb_CheckedChanged1;

				//flowPanelCardTypes1.Controls.Add(rb);
			}

			// panel 2
			foreach (CardType type in types)
			{
				RadioButton rb = new RadioButton();
				rb.Tag = type; // Kartentyp im Radiobutton speichern
				rb.Text = type.Name;
				//rb.Image = Image.FromFile(type.PicturePath);
				rb.TextImageRelation = TextImageRelation.ImageAboveText;
				rb.Height = 70;
				rb.Width = 90;

				if (type.ID == 1)
				{
					rb.Checked = true;
					erstellenKartenTyp = type.ID;
				}

				rb.CheckedChanged += Rb_CheckedChanged2;

				//flowPanelCardTypes2.Controls.Add(rb);
			}
		}

		private void ReportSuccess(string message, Label labelToEdit)
		{
			labelToEdit.Text = message;
			labelToEdit.ForeColor = Color.Green;
			richTextBox2.AppendText("\nErfolg: " + message);
		}

		private void ReportFault(string message, Label labelToEdit)
		{
			labelToEdit.Text = message;
			labelToEdit.ForeColor = Color.Red;
			richTextBox2.AppendText("\nFehler: " + message);
		}

		private void Rb_CheckedChanged1(object sender, EventArgs e)
		{
			RadioButton rb = (RadioButton)sender;
			CardType type = (CardType)rb.Tag;
			pruefenKartenTyp = type.ID;
		}

		private void Rb_CheckedChanged2(object sender, EventArgs e)
		{
			RadioButton rb = (RadioButton)sender;
			CardType type = (CardType)rb.Tag;
			erstellenKartenTyp = type.ID;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			DateOfExpiry.Focus();
		}
	}
}
