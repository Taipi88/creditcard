﻿1. Monat
	Die Prüfung auf gültige Monate im SQL ist als CHECK-Constraint definiert(> 0, < 13)
2. Jahr
	Die Prüfung auf gültige, zweistellige Jahre im SQL ist als CHECK-Constraint definiert(> 0, <= 99)
3. CVV
	Die Prüfung auf eine 4-stellige CVV erfoglt auch als CHECK-Constraint mit Hilfe der SQL-Funktion LEN
	Des Weiteren wurde die Spalte zwar als char-Spalte beibehalten - im Constraint jedoch sind nur Nummern erlaubt
4. Kartennummer
	Kreditkartennummer sind 16-stellig - Prüfung äquivalent zu CVV - nur eben mit 16 Stellen (Punkte & Co gibts an der Stelle nicht)
	Des Weiteren wurde die Spalte zwar als char-Spalte beibehalten - im Constraint jedoch sind nur Nummern erlaubt
5. NULL-Werte
	Da alle Angaben Pflichtangaben sind erlaubt keine Spalte NULL-Werte
6. Sicherheitsaspekte
	Niemand (außer dem Admin) sollte Schreibrechte auf die DB haben - die Anwendung selbst nutzt die StoredProcedure sp_setapprole um sich
	mit der Datenbank zu verbinden. Name der Anwendungsrolle: 'Readers', Kennweort: 'nicole', diese Rolle hat Leserechte und Executerechte auf die DB,
	Änderungen an der DB können somit vom bösen Admin einmal abgesehen nur über die StoredProcedures erfolgen.
	(Siehe https://msdn.microsoft.com/en-us/library/ms188908.aspx)
	Damit das ganze mit .NET funktioniert muss ConnectionPooling (siehe ConnectionString) deaktiviert werden, da Verbindungen mit dem SQL-Server normalerweise automatisch
	gehalten werden. Siehe http://stackoverflow.com/questions/556494/how-can-i-detect-condition-that-causes-exception-before-it-happens
