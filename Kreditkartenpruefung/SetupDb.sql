﻿-- ACHTUNG: Zuerst CreateDb.sql ausführen
-- jeweils über 'Skripts generieren' erstellt worden!

USE Credit;

-- Rolle anpassen für das richtige PW
ALTER APPLICATION ROLE Readers 
    WITH NAME = Readers, 
    PASSWORD = 'nicole#123';
GO

-- Rolle anpassen für die richtigen Rechte
GRANT EXECUTE TO Readers
GRANT SELECT TO Readers

-- Kreditkartentypen einpflegen
INSERT INTO Cardtype (Cardtype_Name) VALUES ('Master Card') --id 1
INSERT INTO Cardtype (Cardtype_Name) VALUES ('Visa') --id 2
INSERT INTO Cardtype (Cardtype_Name) VALUES ('Diners Club') --id 3
INSERT INTO Cardtype (Cardtype_Name) VALUES ('Maestro') --id 4

-- Beispielkunde anlegen
INSERT INTO Customer (Customer_Name, Customer_Surname) VALUES ('Nicole', 'Graf')

-- Beispielkarte anlegen (mittels StoredProcecdure für zufällige Nummer und CVV)
DECLARE @RC int
DECLARE @Customer_ID int = 1 -- Nicole Graf
DECLARE @Cardtype_ID int = 1 -- MasterCard
DECLARE @Expiry char(5) = '08/19'
DECLARE @Card_ID int

EXECUTE @RC = [dbo].[SaveCard] 
   @Customer_ID
  ,@Cardtype_ID
  ,@Expiry
  ,@Card_ID OUTPUT
GO
