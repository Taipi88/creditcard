﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreditkartenpruefung.Data;

namespace KreditkartenpruefungTests
{
	class Program
	{
		static void Main(string[] args)
		{
			CheckAuthorization();
			CheckWriteAccess();
			CheckSave();

			Console.ReadLine();
		}

		static void CheckAuthorization()
		{
			DatenbankHelfer db = new DatenbankHelfer();
			bool authOK = db.AuthorizeTest();
			Console.WriteLine("Login für Anwendungsrolle 'Readers' mit Passwort 'nicole' war erfolgreich?\n->Ergebnis: {0}", authOK);
		}

		static void CheckWriteAccess()
		{
			DatenbankHelfer db = new DatenbankHelfer();
			bool writeFailed = db.WriteTest();
			Console.WriteLine("Direkter Schreibvorgang wie erwartet schief gegangen?\n->Ergebnis: {0}", writeFailed);
		}

		static void CheckSave()
		{
			DatenbankHelfer db = new DatenbankHelfer();
			CardDetails details = db.SaveNewCard("Nicole", "Graf", 1, "03/16");
			if (details != null)
			{
				Console.WriteLine("-> Kartennummer: {0}", details.Number);
				Console.WriteLine("-> CVV: {0}", details.CVV);
			}
		}
	}
}
